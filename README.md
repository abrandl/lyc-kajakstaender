# LYC Kajakständer

## Ansicht

[PDF](3d.pdf)

![3d](3d.svg)

## Plan

[PDF](plan.pdf)

![plan](plan.svg)

## Material

[Sheet](https://docs.google.com/spreadsheets/d/1z2qhid8skCXWpKI9fgBxaTeWpZZU3rTZ7SJNSNOAGEo/edit?usp=sharing)
